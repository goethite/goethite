{
    inputs.pebble.url = "github:Sorixelle/pebble.nix";
    inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixos-21.05";
    inputs.flake-utils.url = "github:numtide/flake-utils";

    outputs = { self, pebble, nixpkgs, flake-utils }:
        flake-utils.lib.eachSystem [ "i686-linux" "x86_64-linux" "x86_64-darwin" ] (system: with nixpkgs.legacyPackages.${system}; rec {
            devShell = pebble.pebbleEnv.${system} {
                cloudPebble = true;
            };
        });
}
